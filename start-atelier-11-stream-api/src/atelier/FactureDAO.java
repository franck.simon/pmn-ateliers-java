package atelier;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FactureDAO {
	private static List<Facture> factures = new ArrayList<>();
	private static int numFacture = 1;
	private static Random random = new Random();
	
	static {
		initFactures();
	}
	
	public static List<Facture> getFactures(){
		return factures;
	}

	private static void initFactures() {
		for(int i=0 ; i<100 ; i++) {
			Facture facture = new Facture();
			String numFact = String.format("FAC23-%05d", numFacture++);
			facture.setNumero(numFact);
			initLignesFacture(facture);
			factures.add(facture);
		}		
	}

	private static void initLignesFacture(Facture facture) {
		int nbLignes = random.nextInt(10)+1;
		for(int i=1 ; i<=nbLignes ; i++) {
			LigneFacture lf = new LigneFacture();
			lf.setProduit("produit "+i);
			lf.setPrix(i*1.2);
			lf.setNombre(i*10);
			facture.add(lf);
		}			
	}
}
