package atelier;

public class LigneFacture {
	private String produit;
	private double prix;
	private double nombre;
	
	public LigneFacture() {}

	public LigneFacture(String produit, double prix, double nombre) {
		this.produit = produit;
		this.prix = prix;
		this.nombre = nombre;
	}

	public String getProduit() {
		return produit;
	}

	public void setProduit(String produit) {
		this.produit = produit;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public double getNombre() {
		return nombre;
	}

	public void setNombre(double nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "LigneFacture [produit=" + produit + ", prix=" + prix + ", nombre=" + nombre + "]";
	}
	
	
	
	
}
