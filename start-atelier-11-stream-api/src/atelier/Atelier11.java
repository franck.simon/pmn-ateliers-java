package atelier;

import java.util.List;

public class Atelier11 {
	public static void main(String[] args) {
		List<Facture> factures = FactureDAO.getFactures();
		factures.forEach(System.out::println);
	}

}
