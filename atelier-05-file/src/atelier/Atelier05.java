package atelier;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import atelier.dao.ContactDAO;
import atelier.dao.ContactJdbcDAO;
import atelier.entity.Contact;
import atelier.exception.DemoDAOException;

public class Atelier05 {
	static String csvFile = "contact.csv";
	static char separateur = ';';
	static String propertiesFile = "/application.properties";

	public static void main(String[] args) throws DemoDAOException, IOException {
		try(InputStream in = Atelier05.class.getResourceAsStream(propertiesFile)){
			Properties props = new Properties();
			props.load(in);
			
			ContactDAO dao = new ContactJdbcDAO(props.getProperty("driver"),
												props.getProperty("url"),
												props.getProperty("user",null),
												props.getProperty("pswd",null));

			System.out.println("=== Ecriture du fichier "+csvFile);
			toCsvFile(dao);
			System.out.println("=== Lecture du fichier "+csvFile);
			readCsvFile();
		}
	}

	public static void toCsvFile(ContactDAO dao) throws IOException, DemoDAOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(csvFile))) {

			List<Contact> contacts = dao.getAllContacts();

			for (Contact c : contacts) {
				StringBuilder sb = new StringBuilder();
				sb.append(c.getCivilite()).append(separateur)
					.append(c.getPrenom()).append(separateur)
					.append(c.getNom()).append('\n');
				bw.write(sb.toString());
			}
		}
	}
		
	private static void readCsvFile() throws FileNotFoundException, IOException {
		try(BufferedReader br = new BufferedReader(new FileReader(csvFile))){
			String line = null;
			while((line = br.readLine()) != null) {
				System.out.println(line);
			}
		}
		
	}
}
