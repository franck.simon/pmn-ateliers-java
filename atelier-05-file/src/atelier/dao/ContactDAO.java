package atelier.dao;

import java.util.List;

import atelier.entity.Contact;
import atelier.exception.DemoDAOException;
/**
 * DAO : Data Access Object
 * 
 * @author Franck
 *
 */
public interface ContactDAO {
	List<Contact> getAllContacts() throws DemoDAOException;
	List<Contact> getContactsByCivilite(String civilite) throws DemoDAOException;
	Contact save(Contact contact) throws DemoDAOException;
	void update(Contact contact) throws DemoDAOException;
	void delete(Contact contact) throws DemoDAOException;
	Contact findById(long id) throws DemoDAOException;
}