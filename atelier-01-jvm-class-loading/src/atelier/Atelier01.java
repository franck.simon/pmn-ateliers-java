package atelier;

public class Atelier01 {

	public static void main(String[] args) {
		Foo foo = new Foo();
		new Thread(new Runnable() {		
			@Override
			public void run() {
				try {
					Thread.sleep(2_000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				new Bar();
				System.out.println(">>> Fin du thread secondaire");		
			}
		}).start();
		foo = null;
		System.out.println(">>> Fin du thread principal");

	}
}