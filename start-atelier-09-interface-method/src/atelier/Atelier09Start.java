package atelier;

public class Atelier09Start {

	public static void main(String[] args) {
		Latitude lat1 = new Latitude(10.0);
		Latitude lat2 = new Latitude(19.0);
		
		if (lat1.compareTo(lat2) == 0) {
			System.out.println("lat1 == lat2");
		}
		if (lat1.compareTo(lat2) > 0) {
			System.out.println("lat1 > lat2");
		}
		if (lat1.compareTo(lat2) < 0) {
			System.out.println("lat1 < lat2");
		}
		
		
// TODO : décommenter et ajouter les méthodes dans l'interface Comparable
//			la classe Latitude n'est pas impactée
//		
//		if(lat1.like(lat2)) {
//			System.out.println("lat1 == lat2");
//		}
//		if(lat1.greaterThan(lat2)) {
//			System.out.println("lat1 > lat2");
//		}
//		if(lat1.lesserThan(lat2)) {
//			System.out.println("lat1 < lat2");
//		}
	}

}
