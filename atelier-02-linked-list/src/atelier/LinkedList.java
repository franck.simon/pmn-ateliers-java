package atelier;

import java.util.Iterator;

public class LinkedList<T> implements Collection<T>{
	private Node first = null;
	private Node last = null;
	
	@Override
	public Iterateur<T> getIterateur() {
		return new LinkedListIterateur();
	}

	@Override
	public void add(T data) {
		if(first==null) {
			first = new Node(data);
			last = first;
			return;
		}
		Node n = new Node(data);
		last.next = n;
		last = n;
	}

	private class Node{
		T data;
		Node next;
		
		Node(T data){
			this.data = data;
			next = null;
		}
	}
	
	private class LinkedListIterateur implements Iterateur<T>{
		Node current = first;

		
		@Override
		public boolean hasNext() {
			return current != null;
		}

		@Override
		public T next() {
			T data = current.data;
			current = current.next;
			return data;
		}
		
	}
	
}
