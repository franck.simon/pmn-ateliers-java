package atelier;

public interface Collection<T>
{
	public Iterateur<T> getIterateur();
	public void add(T item);
}
