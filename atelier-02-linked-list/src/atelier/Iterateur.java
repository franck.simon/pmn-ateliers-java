package atelier;

public interface Iterateur<T>
{
	public boolean hasNext();
	public T next();
}
