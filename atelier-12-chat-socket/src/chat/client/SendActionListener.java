package chat.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Logger;

import chat.ChatMessage;

public class SendActionListener implements ActionListener {
	private static final Logger LOG = Logger.getLogger(SendActionListener.class.getSimpleName());
	private MainChatClient window;

	public SendActionListener(MainChatClient window) {
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		String message = window.tfMessage.getText();
		String name = window.tfName.getText().strip().isEmpty() == true ? "unknow" : window.tfName.getText();
		LOG.info(name + " : " + message);
		if (window.isConnected()) {
			try {
				System.out.println("send");
				ChatMessage chatMessage = new ChatMessage(name, message);
				window.out.writeObject(chatMessage);
				window.out.flush();
				//String messages = window.messagesList.getText();
			} catch (IOException e) {
				LOG.severe(e.getMessage());
			}
		}
	}

}
