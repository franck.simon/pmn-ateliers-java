package chat.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Logger;

import chat.ChatMessage;

public class ConnectionActionListener implements ActionListener {
	private static final Logger LOG = Logger.getLogger(ConnectionActionListener.class.getSimpleName());
	private MainChatClient window;
	private ClientThread clientThread;

	public ConnectionActionListener(MainChatClient window) {
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO : gérer la connexion et la déconnexion
		// maintenir la variable connected
		String host = window.tfHost.getText();
		int port = Integer.parseInt(window.tfPort.getText());
		try {
			if (window.isConnected()) {
				window.socket.close();
				window.in = null;
				window.out = null;
				window.setConnected(false);
				window.infos.setText("Déconnecté");
				clientThread.interrupt();
				LOG.info(String.format("DISCONNECTED FROM Host : %s - port %d\n", host, port));
			} else {
				window.socket = new Socket(host, port);
				window.out = new ObjectOutputStream(window.socket.getOutputStream());
				window.in = new ObjectInputStream(window.socket.getInputStream());
				window.setConnected(true);
				window.infos.setText("Connecté à " + host + ":" + port);
				clientThread = new ClientThread();
				clientThread.start();
				LOG.info(String.format("CONNECTED TO Host : %s - port %d\n", host, port));
			}
		} catch (IOException e) {
			LOG.severe(e.getMessage());
		}

	}
	
	private class ClientThread extends Thread{
		@Override
		public void run() {
			LOG.info("Début du thread écoute serveur");
			while(!Thread.currentThread().isInterrupted()) {
				try {
					ChatMessage chatMessage = (ChatMessage) window.in.readObject();
					String messages = window.messagesList.getText();
					window.messagesList.setText(messages+"\n"+chatMessage);
				} catch (ClassNotFoundException | IOException e) {
					LOG.severe(e.getMessage());
				}
				
			}
			LOG.info("Fin du thread écoute serveur");
		}
	}

}
