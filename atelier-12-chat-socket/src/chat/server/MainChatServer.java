package chat.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import chat.ChatMessage;

public class MainChatServer {
	private static final Logger LOG = Logger.getLogger(MainChatServer.class.getCanonicalName());
	
	private class ClientConnection extends Thread {
		private Socket socket;
		private ObjectInputStream in;
		private ObjectOutputStream out;

		public ClientConnection(Socket clientSocket) {
			this.socket = clientSocket;
			try {
				this.out = new ObjectOutputStream(socket.getOutputStream());
				this.in = new ObjectInputStream(socket.getInputStream());
			} catch (IOException e) {
				LOG.severe(e.getMessage());
			}
			this.start();
		}

		@Override
		public void run() {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					ChatMessage chatMessage = (ChatMessage) in.readObject();
					LOG.info(">>> received : " + chatMessage + " from " + socket);
					sendMessageToAll(chatMessage);
				}
			} catch (IOException | ClassNotFoundException e) {
				LOG.log(Level.SEVERE,"ERREUR RECEPTION",e);
			} finally {
				connectedClients.remove(this);
			}

		}

		public void sendMessage(ChatMessage chatMessage) throws IOException {
			out.writeObject(chatMessage);
			out.flush();
		}
		
		private void sendMessageToAll(ChatMessage chatMessage) throws IOException {
			for (ClientConnection c : connectedClients) {
				c.sendMessage(chatMessage);
			}
		}

	}

	private List<MainChatServer.ClientConnection> connectedClients;
	private int port = 8888;
	private ServerSocket serverSocket;

	public MainChatServer(int port) {
		this();
		this.port = port;
	}

	public MainChatServer() {
		connectedClients = new ArrayList<MainChatServer.ClientConnection>();
	}

	public void start() {
		Socket clientSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			LOG.info(">>> serveur démarré sur le port " + port);
			while (!Thread.currentThread().isInterrupted()) {
				clientSocket = serverSocket.accept();
				LOG.info("=> Client accepté : " + clientSocket.toString());
				ClientConnection client = new ClientConnection(clientSocket);
				connectedClients.add(client);
			}
		} catch (IOException e) {
			LOG.log(Level.SEVERE,">>> ERREUR DEMARRAGE SERVEUR",e);
		}
	}


	public static void main(String[] args) {
		MainChatServer serverChat = new MainChatServer();
		serverChat.start();
	}
}
