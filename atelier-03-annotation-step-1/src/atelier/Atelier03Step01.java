package atelier;

import atelier.conteneur.Container;
import atelier.metier.CompteBanque;
import atelier.metier.ICompte;

public class Atelier03Step01 {

	public static void main(String[] args) throws Exception {

		Container container = new Container();
		container.put("compte", CompteBanque.class);

		ICompte compte = (ICompte) container.get("compte");
		compte.debiter(100);
	}

}
