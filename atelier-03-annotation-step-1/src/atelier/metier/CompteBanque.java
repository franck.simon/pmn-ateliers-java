package atelier.metier;

import atelier.annotations.Managed;

@Managed
public class CompteBanque implements ICompte {

	@Override
	public boolean debiter(double somme) {
		System.out.printf(">>> DEBIT DE LA SOMME DE %.2f €\n",somme);
		return true;
	}

}
