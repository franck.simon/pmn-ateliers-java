package atelier.conteneur;

import java.util.HashMap;
import java.util.Map;

import atelier.annotations.Managed;


public class Container {
	private Map<String, Object> map = new HashMap<String, Object>();

	public void put(String name, Class<?> clazz) throws Exception {
		if (clazz.isAnnotationPresent(Managed.class)) {
			Object o = clazz.getDeclaredConstructor().newInstance();
			map.put(name, o);
		}
	}
	
	public void put(Class<?> clazz) throws Exception {
		if (clazz.isAnnotationPresent(Managed.class)) {
			Managed managed = clazz.getAnnotation(Managed.class);
			String name = managed.name();
			Object o = clazz.getDeclaredConstructor().newInstance();
			if(name.isEmpty()) {
				name = name.substring(0,1).toLowerCase()+name.substring(1);
			}
			map.put(name, o);
		}
	}

	
	public Object get(String name) {
		return map.get(name);
	}
}
