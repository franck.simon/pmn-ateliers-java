package atelier.jmx;

public interface StringListProbeMBean {
	int size();
	void clear();
}
