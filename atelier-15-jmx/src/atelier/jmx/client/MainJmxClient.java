package atelier.jmx.client;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.management.InstanceNotFoundException;
import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectionNotification;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import atelier.jmx.StringListProbe;
import atelier.jmx.StringListProbeMBean;

public class MainJmxClient {
	private static StringListProbeMBean probe;
	
	public static void main(String[] args) throws IOException, MalformedObjectNameException, InstanceNotFoundException {
		JMXServiceURL serviceURL = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://:3333/jmxrmi");
		JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceURL);
		MBeanServerConnection serverConnection = jmxConnector.getMBeanServerConnection();
		
		ObjectName objectName = new ObjectName("atelier.jmx:type=probe");
		probe = JMX.newMBeanProxy(serverConnection, objectName, StringListProbeMBean.class, true);
		
		serverConnection.addNotificationListener(objectName, new ProbeListener(), null, null);
		
		System.out.println(">>> Client JMX démarré");
		
		while(true)
			;
	}
}

class ProbeListener implements NotificationListener{
	
	@Override
	public void handleNotification(Notification notification, Object handback) {
		System.out.println(">>> notification "+notification);
		
	}
}
