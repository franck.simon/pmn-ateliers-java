package atelier.jmx;

import java.lang.management.ManagementFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.Notification;
import javax.management.ObjectName;

import atelier.StringList;

public class ProbeServer extends Thread{
	private static final Logger LOG = Logger.getLogger(ProbeServer.class.getCanonicalName());
	private StringList liste;
	private int nbMaxElements;
	private final int msdelay = 500;
	private int seqNumber = 0;
	
	public ProbeServer(StringList liste, int nbElements) {
		this.liste = liste;
		this.nbMaxElements = nbElements;
	}
	
	
	@Override
	public void run() {
		try {
			MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();
			ObjectName name = new ObjectName("atelier.jmx:type=probe");
			StringListProbe mBean = new StringListProbe(liste, nbMaxElements);
			beanServer.registerMBean(mBean, name);
			LOG.info(">>> MBeanServer started");
			while(!Thread.currentThread().isInterrupted()) {
				Thread.sleep(msdelay);
				if(liste.size() > nbMaxElements) {
					Notification notif = new Notification("atelier.jmx.maxElements", mBean, ++seqNumber);			
					mBean.sendNotification(notif);
				}
			}
			beanServer.unregisterMBean(name);
		}catch(MalformedObjectNameException | InstanceAlreadyExistsException | MBeanRegistrationException 
											| NotCompliantMBeanException | InterruptedException | InstanceNotFoundException e) {
			LOG.log(Level.SEVERE,"ERREUR", e);
		}finally {
			LOG.info(">>> MBeanServer stopped");
		}
	}
	
	
}
