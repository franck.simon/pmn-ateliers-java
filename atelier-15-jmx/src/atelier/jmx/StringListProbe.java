package atelier.jmx;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.NotificationBroadcasterSupport;

import atelier.StringList;

public class StringListProbe extends NotificationBroadcasterSupport implements StringListProbeMBean {
	private StringList liste;
	public StringListProbe(StringList liste, int maxElements) {
		this.liste = liste;
	}

	@Override
	public int size() {
		return liste.size();
	}

	@Override
	public void clear() {
		liste.clear();
	}

	@Override
	public MBeanNotificationInfo[] getNotificationInfo() {
		String[] types = new String[] {"atelier.jmx.maxElements"};
		String name = AttributeChangeNotification.class.getName();
		String description = "Nombre maximum d'éléments atteind";
		MBeanNotificationInfo info = new MBeanNotificationInfo(types, name, description);
		return new MBeanNotificationInfo[] {info};
	}

}
