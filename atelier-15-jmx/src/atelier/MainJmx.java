package atelier;

import atelier.jmx.ProbeServer;

public class MainJmx {

	public static void main(String[] args) {
		StringList liste = new StringList();
		new ProbeServer(liste, 10).start();
		new Thread(()->{
						for (int i = 0; i < 1_000; i++) {
							liste.add("i == " + i);
							System.out.println("ajout de 'i== "+i+"'");
							try {
								Thread.sleep(2_000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
		}).start();

		while (true)
			;
	}

}
