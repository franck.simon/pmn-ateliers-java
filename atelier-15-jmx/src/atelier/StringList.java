package atelier;

import java.util.ArrayList;
import java.util.List;

public class StringList {
	private List<String> liste = new ArrayList<String>();
	
	public void add(String element) {
		liste.add(element);
	}
	
	public String get(int pos) {
		return liste.get(pos);
	}
	
	public int size() {
		return liste.size();
	}
	
	public void clear() {
		liste.clear();
	}

}
