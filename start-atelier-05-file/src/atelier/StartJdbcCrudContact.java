package atelier;

import java.util.List;

import atelier.dao.ContactDAO;
import atelier.dao.ContactJdbcDAO;
import atelier.entity.Contact;
import atelier.exception.DemoDAOException;

public class StartJdbcCrudContact {

	public static void main(String[] args) throws DemoDAOException {
		ContactDAO dao = new ContactJdbcDAO();
		
		Contact c = dao.findById(1_000);
		if(c!=null) {
			System.out.println(c.getNom());
		}

	}
}
