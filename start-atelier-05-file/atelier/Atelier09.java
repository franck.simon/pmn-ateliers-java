package atelier;

public class Atelier09 {

	public static void main(String[] args) {
		Latitude lat1 = new Latitude(10.0);
		Latitude lat2 = new Latitude(19.0);

		if (lat1.like(lat2)) {
			System.out.println("lat1 == lat2");
		}
		if (lat1.greaterThan(lat2)) {
			System.out.println("lat1 > lat2");
		}
		if (lat1.lesserThan(lat2)) {
			System.out.println("lat1 < lat2");
		}
	}

}
