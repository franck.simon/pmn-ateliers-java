package atelier;

public class Latitude implements Comparable<Latitude>{
	private double value;
		
	public Latitude(double d) {
		this.value = d;
	}

	@Override
	public int compareTo(Latitude t) {
		return (int)(value-t.value);
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

}
