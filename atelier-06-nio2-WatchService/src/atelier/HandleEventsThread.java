package atelier;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class HandleEventsThread extends Thread {
	private String pathName;
	private Path stopFile = Paths.get("STOP");

	public HandleEventsThread(String pathName) {
		this.pathName = pathName;
	}

	@Override
	public void run() {
		final Path dir = Paths.get(pathName);
		// récupération d'une instance de WatchService
		try (WatchService watcher = FileSystems.getDefault().newWatchService();) {

			// enregistrement auprès du répertoire
			WatchKey key = dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);

			System.out.println("=== START WatchService");
			while (!this.isInterrupted()) {
				key = watcher.poll();
				if (key != null) {
					for (final WatchEvent<?> evt : key.pollEvents()) {
						final Path name = (Path) evt.context();
						System.out.format(evt.kind() + " " + "%s\n", name);
						if(name.equals(stopFile)) {
							System.out.println(">>> interruption demandée");
							this.interrupt();
						}
					}
					key.reset();
				}
			}
			System.out.println(">>> END WatchService");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
