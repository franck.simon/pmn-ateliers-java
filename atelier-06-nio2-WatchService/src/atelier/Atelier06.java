package atelier;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class Atelier06 {

	public static void main(String[] args) throws IOException {
		HandleEventsThread th = new HandleEventsThread("/home/franck/tmp");
		th.start();
		System.out.println(">>> Fin thread principal");
	}

}
