package chat.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

public class ConnectionActionListener implements ActionListener{
	private static final Logger LOG = Logger.getLogger(ConnectionActionListener.class.getSimpleName());
	private MainChatClient window;
	
	public ConnectionActionListener(MainChatClient window) {
		this.window = window;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO : gérer la connexion et la déconnexion
		//	maintenir la variable connected
		String host = window.tfHost.getText();
		int port = Integer.parseInt(window.tfPort.getText());
		LOG.info(String.format("Host : %s - port %d\n", host,port));		
	}

}
