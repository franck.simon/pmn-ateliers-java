package chat.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

public class SendActionListener implements ActionListener {
	private static final Logger LOG = Logger.getLogger(SendActionListener.class.getSimpleName());
	private MainChatClient window;
	
	public SendActionListener(MainChatClient window) {
		this.window = window;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		String message = window.tfMessage.getText();
		String name = window.tfName.getText().strip().isEmpty()==true?"unknow":window.tfName.getText();
		String messages = window.messagesList.getText();
		window.messagesList.setText(messages+'\n'+name+" : "+message);
		LOG.info(name+" : "+message);
	}

}
