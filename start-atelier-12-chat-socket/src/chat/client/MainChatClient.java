package chat.client;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.AbstractListModel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class MainChatClient {
	
	private static final Logger LOG = Logger.getLogger(MainChatClient.class.getSimpleName());

	JFrame frame;
	JTextField tfName;
	JTextField tfHost;
	JTextField tfPort;
	JTextField tfMessage;
	JTextArea messagesList;
	
	protected boolean connected = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainChatClient window = new MainChatClient();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainChatClient() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 585, 361);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Chat");
		frame.getContentPane().setLayout(null);
		
		JLabel labelInfos = new JLabel("Infos : ");
		labelInfos.setBounds(12, 12, 553, 15);
		frame.getContentPane().add(labelInfos);
		
		JLabel lblNewLabel_1 = new JLabel("Name");
		lblNewLabel_1.setBounds(12, 38, 70, 15);
		frame.getContentPane().add(lblNewLabel_1);
		
		tfName = new JTextField();
		tfName.setBounds(65, 38, 207, 19);
		frame.getContentPane().add(tfName);
		tfName.setColumns(10);
		
		JLabel lblHost = new JLabel("Host");
		lblHost.setBounds(12, 65, 70, 15);
		frame.getContentPane().add(lblHost);
		
		tfHost = new JTextField();
		tfHost.setText("127.0.0.1");
		tfHost.setBounds(65, 65, 114, 19);
		frame.getContentPane().add(tfHost);
		tfHost.setColumns(10);
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(197, 65, 70, 15);
		frame.getContentPane().add(lblPort);
		
		tfPort = new JTextField();
		tfPort.setText("8888");
		tfPort.setBounds(238, 65, 70, 19);
		frame.getContentPane().add(tfPort);
		tfPort.setColumns(10);
		
		JButton btnConnection = new JButton("CONNEXION");
		btnConnection.addActionListener(new ConnectionActionListener(this));
		btnConnection.setBounds(337, 62, 117, 25);
		frame.getContentPane().add(btnConnection);
		
		JLabel lblNewLabel = new JLabel("Message");
		lblNewLabel.setBounds(12, 95, 70, 15);
		frame.getContentPane().add(lblNewLabel);
		
		tfMessage = new JTextField();
		tfMessage.setBounds(85, 95, 386, 19);
		frame.getContentPane().add(tfMessage);
		tfMessage.setColumns(10);
		
		JButton btnSend = new JButton("SEND");
		btnSend.addActionListener(new SendActionListener(this));
		btnSend.setBounds(476, 92, 79, 25);
		frame.getContentPane().add(btnSend);
		
		JLabel lblMessagesReus = new JLabel("Messages reçus");
		lblMessagesReus.setBounds(12, 122, 167, 15);
		frame.getContentPane().add(lblMessagesReus);
		
		messagesList = new JTextArea();
		messagesList.setEditable(false);
		messagesList.setBounds(12, 139, 553, 185);
		frame.getContentPane().add(messagesList);
		messagesList.setColumns(10);
		
	}
}
