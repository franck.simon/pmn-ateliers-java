package atelier;

import java.io.Serializable;

/**
 * Entité Contact
 * POJO (Plain Old Java Object)
 * 
 * @author Franck
 *
 */
public class Contact implements Serializable, Comparable<Contact>{
	/**
	 * id est le reflet de la clé primaire en base
	 * si id==0 le contact n'a pas été sauvegardé
	 */
	private long id;
	private String civilite;
	private String nom;
	private String prenom;
	
	public Contact() {}
	
	public Contact(String civilite, String nom, String prenom) {
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", civilite=" + civilite + ", nom=" + nom + ", prenom=" + prenom + "]";
	}

	@Override
	public int compareTo(Contact t) {
		if(t.id == id)
			return 0;
		return this.nom.compareToIgnoreCase(t.getNom());
	}
	
}
