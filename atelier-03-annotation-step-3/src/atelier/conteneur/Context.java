package atelier.conteneur;

import atelier.metier.User;

public class Context {
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
