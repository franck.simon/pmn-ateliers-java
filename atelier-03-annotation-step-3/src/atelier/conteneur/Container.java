package atelier.conteneur;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import atelier.annotations.AllowedRoles;
import atelier.annotations.Managed;


public class Container {
	private Context context;
	Map<String, Object> map = new HashMap<String, Object>();

	public Container(Context context) {
		this.context = context;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	public void put(Class<?> clazz) throws Exception {
		String name = clazz.getSimpleName();
		name = name.substring(0,1).toLowerCase()+name.substring(1);
		put(name,clazz);
	}

	public void put(String name, Class<?> clazz) throws Exception {
		if (clazz.getAnnotation(Managed.class) != null) {
			Object o = clazz.getDeclaredConstructor().newInstance();
			boolean proxyNeeded = false;
			for (Method m : o.getClass().getMethods()) {
				if (m.isAnnotationPresent(AllowedRoles.class)) {
					proxyNeeded = true;
					break;
				}
			}
			if (proxyNeeded) {
				Class<?>[] interfaces = o.getClass().getInterfaces();
				AuthentificationProxyHandler handler = new AuthentificationProxyHandler(context, o);
				Object proxy = Proxy.newProxyInstance(o.getClass().getClassLoader(), interfaces, handler);
				map.put(name, proxy);
			} else {
				map.put(name, o);
			}
		}
	}

	public Object get(String name) {
		return map.get(name);
	}
	
	public <T> T get(String name,Class<T> clazz) {
		return (T) map.get(name);
	}
	
}
