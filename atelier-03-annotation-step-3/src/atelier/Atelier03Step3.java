package atelier;

import atelier.conteneur.Container;
import atelier.conteneur.Context;
import atelier.metier.CompteBanque;
import atelier.metier.ICompte;
import atelier.metier.NotAllowedRole;
import atelier.metier.User;

public class Atelier03Step3 {

	public static void main(String[] args) throws Exception {

		Context context = new Context();
		context.setUser(new User("toto", "totop"));

		Container container = new Container(context);
		container.put("compte", CompteBanque.class);
		
		
		ICompte compte = (ICompte) container.get("compte");
		
//		container.put(CompteBanque.class);
//		ICompte compte = container.get("compteBanque",ICompte.class);
		
		try {
			compte.debiter(100);
		} catch (NotAllowedRole e) {
			System.out.println("Erreur d'authentification");
		}
		compte.getSolde();
	}

}
