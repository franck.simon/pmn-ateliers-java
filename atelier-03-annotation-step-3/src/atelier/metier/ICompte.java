package atelier.metier;

public interface ICompte {
	public boolean debiter(double somme) throws NotAllowedRole;
	public double getSolde();
}
