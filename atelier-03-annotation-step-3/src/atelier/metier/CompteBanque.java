package atelier.metier;

import atelier.annotations.AllowedRoles;
import atelier.annotations.Managed;

@Managed
public class CompteBanque implements ICompte {

	@Override
	@AllowedRoles(allowed={"admin"})
	public boolean debiter(double somme) {
		System.out.printf(">>> DEBIT DE LA SOMME DE %.2f €\n",somme);
		return true;
	}

	@Override
	public double getSolde() {
		return 1000.0;
	}

}
