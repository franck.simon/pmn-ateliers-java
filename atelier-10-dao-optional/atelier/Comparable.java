package atelier;

public interface Comparable<T> {
	/**
	 * 
	 * @param t
	 * 		instance t à comparer avec this
	 * @return
	 * 		0 si les deux objets sont égaux
	 * 		>0 si l'instance this est supérieure à t
	 * 		<0 si l'instance this est inférieure à t
	 */
	int compareTo(T t);
	
	default boolean like(T t) {
		return compareTo(t)==0;
	}
	
	default boolean greaterThan(T t) {
		return compareTo(t)>0;
	}
	
	default boolean lesserThan(T t) {
		return compareTo(t)<0;
	}
}
