package atelier.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import atelier.entity.Contact;
import atelier.exception.DemoDAOException;
/**
 * Implémentation JDBC de ContactDAO
 * @author Franck
 *
 */
public class ContactJdbcDAO implements ContactDAO {
	private String driver = "org.apache.derby.jdbc.ClientDriver40";
	private String url = "jdbc:derby://localhost:1527/contacts";
	private String user = null;
	private String pswd = null;
	
	public ContactJdbcDAO() throws DemoDAOException {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			throw new DemoDAOException(e);
		}
	}
	
	
	@Override
	public List<Contact> getAllContacts() throws DemoDAOException{
		List<Contact> contacts = new ArrayList<>();
		String sql = "SELECT * FROM personnes";
		try(Connection con = DriverManager.getConnection(url, user, pswd)) {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Contact c = createContact(rs);
				contacts.add(c);
			}
		} catch (SQLException e) {
			throw new DemoDAOException(e);
		}	
		return contacts;
	}


	private Contact createContact(ResultSet rs) throws SQLException {
		Contact c = new Contact();
		c.setId(rs.getLong("pk"));
		c.setCivilite(rs.getString("civilite"));
		c.setNom(rs.getString("nom"));
		c.setPrenom(rs.getString("prenom"));
		return c;
	}


	@Override
	public Contact save(Contact contact) throws DemoDAOException {
		String sql = "INSERT INTO personnes(civilite,nom,prenom) VALUES (?,?,?)";
		try(Connection con = DriverManager.getConnection(url, user, pswd)) {
			PreparedStatement ps = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, contact.getCivilite());
			ps.setString(2, contact.getNom());
			ps.setString(3, contact.getPrenom());
			int count = ps.executeUpdate();
			System.out.println(">>> "+count);
			
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next()) {
				contact.setId(rs.getLong(1));
			}else {
				throw new DemoDAOException("Erreur lors de l'insertion");
			}
		} catch (SQLException e) {
			throw new DemoDAOException(e);
		}
		return contact;
		
	}


	@Override
	public void update(Contact contact) throws DemoDAOException {
		String sql = "UPDATE personnes SET civilite=?,nom=?,prenom=? WHERE pk=?";
		try(Connection con = DriverManager.getConnection(url, user, pswd)) {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, contact.getCivilite());
			ps.setString(2, contact.getNom());
			ps.setString(3, contact.getPrenom());
			ps.setLong(4, contact.getId());
			int count = ps.executeUpdate();
			System.out.println(">>> "+count);
		} catch (SQLException e) {
			throw new DemoDAOException(e);
		}
	}


	@Override
	public void delete(Contact contact) throws DemoDAOException {
		String sql = "DELETE FROM personnes WHERE pk=?";
		try(Connection con = DriverManager.getConnection(url, user, pswd)) {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, contact.getId());
			int count = ps.executeUpdate();
			contact.setId(0);// indique que l'instance n'est plus en base
			System.out.println(">>> "+count);
		} catch (SQLException e) {
			throw new DemoDAOException(e);
		}
		
	}


	@Override
	public List<Contact> getContactsByCivilite(String civilite) throws DemoDAOException {
		String sql = "SELECT * FROM personnes WHERE civilite=?";
		List<Contact> contacts = new ArrayList<>();
		
		try(Connection con = DriverManager.getConnection(url, user, pswd)) {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, civilite);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Contact c = createContact(rs);
				contacts.add(c);
			}
		} catch (SQLException e) {
			throw new DemoDAOException(e);
		}	
		
		return contacts;
	}


	@Override
	public Contact findById(long id) throws DemoDAOException {
		String sql = "SELECT * FROM personnes WHERE pk=?";
		try(var con = DriverManager.getConnection(url, user, pswd)) {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				Contact c = createContact(rs);
				return c;
			}
		} catch (SQLException e) {
			throw new DemoDAOException(e);
		}	
		return null;
	}


	@Override
	public Optional<Contact> getById(long id) throws DemoDAOException {
		return Optional.ofNullable(findById(id));
		
//		String sql = "SELECT * FROM personnes WHERE pk=?";
//		try(var con = DriverManager.getConnection(url, user, pswd)) {
//			PreparedStatement ps = con.prepareStatement(sql);
//			ps.setLong(1, id);
//			ResultSet rs = ps.executeQuery();
//			if(rs.next()) {
//				Contact c = createContact(rs);
//				return Optional.of(c);
//			}
//		} catch (SQLException e) {
//			throw new DemoDAOException(e);
//		}	
//		return Optional.empty();
	}
	
	
	
	
}
