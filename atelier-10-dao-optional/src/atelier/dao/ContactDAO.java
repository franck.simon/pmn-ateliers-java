package atelier.dao;

import java.util.List;
import java.util.Optional;

import atelier.entity.Contact;
import atelier.exception.DemoDAOException;
/**
 * DAO : Data Access Object
 * 
 * @author Franck
 *
 */
public interface ContactDAO {
	List<Contact> getAllContacts() throws DemoDAOException;
	List<Contact> getContactsByCivilite(String civilite) throws DemoDAOException;
	Contact save(Contact contact) throws DemoDAOException;
	void update(Contact contact) throws DemoDAOException;
	void delete(Contact contact) throws DemoDAOException;
	
	/**
	 * 
	 * @deprecated seulement pour usage privé
	 * 		cette méthode sera supprimée et remplacée par {@link #getById(long)}
	 */
	@Deprecated(forRemoval = true, since = "10")
	Contact findById(long id) throws DemoDAOException;
	
	Optional<Contact> getById(long id) throws DemoDAOException;
}