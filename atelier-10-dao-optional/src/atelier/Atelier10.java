package atelier;

import java.util.List;
import java.util.Optional;

import atelier.dao.ContactDAO;
import atelier.dao.ContactJdbcDAO;
import atelier.entity.Contact;
import atelier.exception.DemoDAOException;

public class Atelier10 {

	public static void main(String[] args) throws DemoDAOException {
		ContactDAO dao = new ContactJdbcDAO();
		
		Optional<Contact> opt = dao.getById(1_000);
		opt.ifPresent(c->System.out.println(c.getNom()));

	}
}
