package atelier.jms.consumer;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import atelier.jms.Sensor;

public class AtelierJmsQueueAsyncConsumer {

	public static void main(String[] args) throws NamingException, JMSException {
		Context ctx = new InitialContext();

		ConnectionFactory factory = (ConnectionFactory) ctx.lookup("ConnectionFactory");

		Connection connection = factory.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = (Destination) ctx.lookup("sensors");

		MessageConsumer consumer = session.createConsumer(destination);
		consumer.setMessageListener(new MessageListener() {
			@Override
			public void onMessage(Message message) {
				TextMessage textMessage = (TextMessage) message;
				try {
					ObjectMapper om = new ObjectMapper();
					Sensor sensor = om.readValue(textMessage.getText(), Sensor.class);
					System.out.println("=== Réception message : " + sensor);
				} catch (JMSException | JsonProcessingException e) {
					e.printStackTrace();
				}
			}
		});

	}

}
