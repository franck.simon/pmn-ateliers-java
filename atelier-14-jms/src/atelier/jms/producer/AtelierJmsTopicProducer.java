package atelier.jms.producer;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import atelier.jms.Command;
import atelier.jms.Command.State;
import atelier.jms.Sensor;

public class AtelierJmsTopicProducer {

	public static void main(String[] args) throws NamingException, JMSException, JsonProcessingException {
		Context ctx = new InitialContext();

		ConnectionFactory factory = (ConnectionFactory) ctx.lookup("ConnectionFactory");
		Command cmd = new Command(State.TOGGLE);
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString(cmd);
		try (Connection connection = factory.createConnection()) {
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			Destination destination = (Destination) ctx.lookup("lamps");

			MessageProducer producer = session.createProducer(destination);
			TextMessage message = session.createTextMessage();
			message.setText(json);
			producer.send(message);
			System.out.println(">>> Message envoyé dans "+destination.toString());
			producer.close();
		}
	}

}
