package atelier.jms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Command {
	public enum State{
		TOGGLE,ON,OFF;
	}
	
	@JsonProperty("command")
	private String state;
	
	public Command() {
		// rien à faire
	}
	
	public Command(State state) {
		this.state = state.toString();
	}



	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return "command : "+state.toString();
	}
	
	
}
