package atelier.jms;

import java.util.Date;

public class Sensor {
	private String id;
	private double value;
	private String unit;
	private long timestamp;
	
	public Sensor() {
		// rien à faire
	}
	
	public Sensor(String id, double value, String unit) {
		this(id,value,unit,new Date().getTime());
	}

	public Sensor(String id, double value, String unit, long timestamp) {
		this.id = id;
		this.value = value;
		this.unit = unit;
		this.timestamp = timestamp;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Sensor [id=" + id + ", value=" + value + ", unit=" + unit + ", timestamp=" + new Date(timestamp) + "]";
	}
	
	
}
