package atelier.jms.consumer;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import atelier.jms.Sensor;

public class AtelierJmsQueueAsyncConsumer {

	public static void main(String[] args) throws NamingException, JMSException {
		// A FAIRE : récupérer le ConnectionFactory dans le contexte JNDI

		// A FAIRE :
		//	- récupérer une Connection auprès du factory
		//	- démarrer la connexion
		//  - créer une session sur la connexion
		//  - créer la destination 

		// A FAIRE :
		//	- créer un MessageConsumer sur la session
		//	- ajouter un listener sur le consommateur
		//	- coder la méthode onMessage() pour afficher sur la console le message reçu
		
		
		// CODE pour transformer le message en objet (dans le listener)
				//			ObjectMapper om = new ObjectMapper();
				//			Sensor sensor = om.readValue(textMessage.getText(), Sensor.class);
				//			System.out.println("=== Réception message : " + sensor);


	}

}
