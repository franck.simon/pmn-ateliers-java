package atelier.jms.producer;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import atelier.jms.Sensor;

public class AtelierJmsQueueProducer {

	public static void main(String[] args) throws NamingException, JMSException, JsonProcessingException {
		// A FAIRE : récupérer le ConnectionFactory dans le contexte JNDI

		// A FAIRE :
		//	- récupérer une Connection auprès du factory
		//	- démarrer la connexion
		//  - créer une session sur la connexion
		//  - créer la destination 
		
		// Création du message à envoyer
		Sensor sensor = new Sensor("sam01", 20.1, "centigrade");
		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString(sensor);
		
		// A FAIRE :
		//	- sur la factory créer une Connection
		//	- sur la connexion créeer une session
		//	- créer une destination
		//	- créer un Message producer et envoyer le json

	}

}
