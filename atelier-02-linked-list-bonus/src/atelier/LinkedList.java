package atelier;

import java.util.Iterator;

public class LinkedList<T> implements Collection<T>{
	private Node first = null;
	
	@Override
	public void add(T data) {
		if(first==null) {
			first = new Node(data);
			return;
		}
		Node n = first;
		while(n.next != null)
			n = n.next;
		n.next = new Node(data);
	}

	private class Node{
		T data;
		Node next;
		
		Node(T data){
			this.data = data;
			next = null;
		}
	}
	
	private class LinkedListIterateur implements Iterator<T>{
		Node current = first;

		
		@Override
		public boolean hasNext() {
			return current != null;
		}

		@Override
		public T next() {
			T data = current.data;
			current = current.next;
			return data;
		}
		
	}

	@Override
	public Iterator<T> iterator() {
		return new LinkedListIterateur();
	}
	
}
