package atelier;

public interface Collection<T> extends Iterable<T>
{
	public void add(T item);
}
