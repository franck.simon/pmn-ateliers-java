package atelier.metier;

public class NotAllowedRole extends Exception {

	private static final long serialVersionUID = 1L;

	public NotAllowedRole() {
		super();
	}

	public NotAllowedRole(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public NotAllowedRole(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public NotAllowedRole(String arg0) {
		super(arg0);
	}

	public NotAllowedRole(Throwable arg0) {
		super(arg0);
	}

}
