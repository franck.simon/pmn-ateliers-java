package atelier.metier;

public class User {
	private String name;
	@SuppressWarnings("unused")
	private String password;
	private String role = "";
	
	public User(String name, String pswd){
		this.name = name;
		this.password = pswd;
		valid();
	}
	private void valid() {
		if(name.equalsIgnoreCase("toto"))
			role = "admin";
		if(name.equalsIgnoreCase("titi"))
			role = "conseiller";
	}

	public String getRole() {
		return role;
	}

}
