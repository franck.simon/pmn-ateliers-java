package atelier.metier;

import atelier.annotations.AllowedRoles;

public interface ICompte {
	@AllowedRoles(allowed={"admin"})
	public boolean debiter(double somme) throws NotAllowedRole;
	public double getSolde();
}
