package atelier.conteneur;

import java.util.HashMap;
import java.util.Map;

import atelier.annotations.Managed;


public class Container {
	Map<String, Object> map = new HashMap<String, Object>();


	public void put(String name, Class<?> clazz) throws Exception {
		if (clazz.isAnnotationPresent(Managed.class)) {
			Object o = clazz.getDeclaredConstructor().newInstance();
			map.put(name, o);
		}
	}

	public Object get(String name) {
		return map.get(name);
	}
	
	
}
