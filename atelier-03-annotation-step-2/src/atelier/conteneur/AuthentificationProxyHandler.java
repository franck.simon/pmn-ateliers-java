package atelier.conteneur;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import atelier.annotations.AllowedRoles;
import atelier.metier.NotAllowedRole;


public class AuthentificationProxyHandler implements InvocationHandler {
	private Object cible;

	public AuthentificationProxyHandler(Object o) {
		this.cible = o;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;
		boolean allowed = false;
		boolean auth = false;
		List<String> roles = null;
		if(method.isAnnotationPresent(AllowedRoles.class)) {
			auth = true;
			roles = Arrays.asList(method.getAnnotation(AllowedRoles.class).allowed());
		}
//		for (Method m : cible.getClass().getMethods()) {
//			if(m.getName().equals(method.getName()) && m.getAnnotation(AllowedRoles.class) != null){
//				auth = true;
//				roles = Arrays.asList(m.getAnnotation(AllowedRoles.class).allowed());
//				break;
//			}
//		}
		if(!auth){
			System.out.println("=> PAS D'AUTHENTIFICATION");
			return method.invoke(cible, args);
		}else {
			System.out.println("=> AUTHENTIFICATION");
			return method.invoke(cible, args);
		}
	}

}
