package atelier;

import java.lang.reflect.Proxy;

import atelier.conteneur.AuthentificationProxyHandler;
import atelier.conteneur.Container;
import atelier.metier.CompteBanque;
import atelier.metier.ICompte;
import atelier.metier.NotAllowedRole;

public class Atelier03Step02 {

	public static void main(String[] args) throws Exception {

//		Container container = new Container();
//		container.put("compte", CompteBanque.class);
//		
//		
//		ICompte compte = (ICompte) container.get("compte");
//		
////		container.put(CompteBanque.class);
////		ICompte compte = container.get("compteBanque",ICompte.class);
//		
//		try {
//			compte.debiter(100);
//		} catch (NotAllowedRole e) {
//			System.out.println("Erreur d'authentification");
//		}
//		compte.getSolde();
		AuthentificationProxyHandler handler = new AuthentificationProxyHandler(new CompteBanque());
		ICompte compte = (ICompte) Proxy.newProxyInstance(ICompte.class.getClassLoader(),
													new Class[] {ICompte.class},
													handler);
		compte.getSolde();
		compte.debiter(100);
	}

}
