package atelier.enums;

import atelier.enums.WidgetRender.Deferred;
import atelier.enums.WidgetRender.FinalRender;
import atelier.enums.WidgetRender.Optimize;
import atelier.enums.WidgetRender.UseCache;

public class UseWidgetRender {

	public static void main(String[] args) {
		WidgetRender render = new WidgetRender();
		Widget widget = new Widget();

		render.show(widget,true,false,false,true);
		
		render.show(widget,
						/*useCache*/ true, 
						/*deferred*/ false, 
						/*optimize*/ false, 
						/*finalRender*/ true);
		
		render.show(widget, UseCache.TRUE, Deferred.FALSE, Optimize.FALSE, FinalRender.TRUE);
	}

}
