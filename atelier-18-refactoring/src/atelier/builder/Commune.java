package atelier.builder;

public class Commune {
	private String nom;
	private String departement;
	private String region;
	private String codePostal;
	private double longitude;
	private double latitude;
	
	public Commune() {
		// rien à faire
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDepartement() {
		return departement;
	}
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public static CommuneBuilder builder() {
		return new CommuneBuilder();
	}
	
	
	public static class CommuneBuilder{
		private Commune commune = new Commune(); 
		
		public CommuneBuilder nom(String nom) {
			commune.nom = nom;
			return this;
		}
		public CommuneBuilder departement(String departement) {
			commune.departement = departement;
			return this;
		}
		public CommuneBuilder region(String region) {
			commune.region = region;
			return this;
		}
		public CommuneBuilder codePostal(String codePostal) {
			commune.codePostal = codePostal;
			return this;
		}
		public CommuneBuilder longitude(double longitude) {
			commune.longitude = longitude;
			return this;
		}
		public CommuneBuilder latitude(double latitude) {
			commune.latitude = latitude;
			return this;
		}
		
		public Commune build() {
			return commune;
		}
		
		
	}
	
}
