package atelier.builder;

public class MainCommuneBuilder {

	public static void main(String[] args) {
		Commune c = Commune.builder().nom("Saint-Malo")
									.departement("Ille-et-Vilaine")
									.codePostal("35400")
									.build();

	}

}
