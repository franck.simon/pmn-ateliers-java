package atelier.start;

public class Commune {
	private String nom;
	private String departement;
	private String region;
	private String codePostal;
	private double longitude;
	private double latitude;
	
	public Commune() {
		// rien à faire
	}
	
	public Commune(String nom) {
		this.nom = nom;
	}

	public Commune(String nom, String codePostal) {
		this.nom = nom;
		this.codePostal = codePostal;
	}

	public Commune(String nom, String departement, String region, String codePostal) {
		this.nom = nom;
		this.departement = departement;
		this.region = region;
		this.codePostal = codePostal;
	}

	public Commune(String nom, String departement, String region, String codePostal, double longitude,
			double latitude) {
		this.nom = nom;
		this.departement = departement;
		this.region = region;
		this.codePostal = codePostal;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDepartement() {
		return departement;
	}
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	@Override
	public String toString() {
		return "Commune [nom=" + nom + ", departement=" + departement + ", region=" + region + ", codePostal="
				+ codePostal + ", longitude=" + longitude + ", latitude=" + latitude + "]";
	}
	
	
}
