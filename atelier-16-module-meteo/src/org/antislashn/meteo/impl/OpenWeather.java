package org.antislashn.meteo.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.antislashn.meteo.Weather;

public class OpenWeather implements Weather {
	private String key = "5d16b4bc887fc4f2634cb0b1d6aee59b";
	@Override
	public String weatherRequest(double latitude, double longitude) {
		StringBuilder sb = new StringBuilder();
		sb.append("https://api.openweathermap.org/data/2.5/weather?lat=")
			.append(latitude)
			.append("&lon=")
			.append(longitude)
			.append("&units=metric&appid=")
			.append(key);
		try {
			HttpRequest request = HttpRequest.newBuilder()
										.uri(new URI(sb.toString()))
										.timeout(Duration.of(5,ChronoUnit.SECONDS))
										.GET()
										.build();
			HttpClient client = HttpClient.newHttpClient();
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			String json = response.body();
			return json;
		} catch (URISyntaxException | IOException | InterruptedException e) {
			return e.getMessage();
		}
	}

}
