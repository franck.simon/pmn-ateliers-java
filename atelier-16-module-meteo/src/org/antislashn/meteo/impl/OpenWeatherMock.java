package org.antislashn.meteo.impl;

import org.antislashn.meteo.Weather;

public class OpenWeatherMock implements Weather{

	@Override
	public String weatherRequest(double latitude, double longitude) {
		return """
				{"temp":21.5}
				""";
	}

}
