package org.antislashn.meteo;

import org.antislashn.meteo.impl.OpenWeather;
import org.antislashn.meteo.impl.OpenWeatherMock;

public interface Weather {
	
	String weatherRequest(double latitude, double longitude);
	
	static Weather getInstance()
	{
		return new OpenWeather();
	}
}
