package atelier.rmi.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import atelier.rmi.Cercle;
import atelier.rmi.Rectangle;
import atelier.rmi.server.FormeCalcul;

public class AtelierRmiClient {

	public static void main(String[] args) throws RemoteException, NotBoundException {
		Registry registry = LocateRegistry.getRegistry();
		FormeCalcul calcul = (FormeCalcul) registry.lookup("calcul");
		Rectangle rectangle = new Rectangle(10.2, 10.4);
		double surfaceRect = calcul.surface(rectangle);
		System.out.println("Surface du rectangle : "+surfaceRect);
		Cercle cercle = new Cercle(10.1);
		double surfaceCercle = calcul.surface(cercle);
		System.out.println("Surface du cercle : "+surfaceCercle);

	}

}
