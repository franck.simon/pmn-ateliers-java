package atelier.rmi;

import java.io.Serializable;

public class Cercle implements Serializable{
	private double rayon;

	public Cercle(double rayon) {
		this.rayon = rayon;
	}

	public double getRayon() {
		return rayon;
	}

	public void setRayon(double rayon) {
		this.rayon = rayon;
	}
	
	

}
