package atelier.rmi.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import atelier.rmi.Cercle;
import atelier.rmi.Rectangle;

public interface FormeCalcul extends Remote{
	double surface(Cercle cercle) throws RemoteException;
	double surface(Rectangle rectangle) throws RemoteException;
}
