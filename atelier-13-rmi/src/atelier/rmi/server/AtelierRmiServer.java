package atelier.rmi.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class AtelierRmiServer {

	public static void main(String[] args) throws RemoteException {
		//System.setProperty("java.rmi.server.codebase", FormeCalculImpl.class.getProtectionDomain().getCodeSource().getLocation().toString());
		FormeCalculImpl fc = new FormeCalculImpl();
		FormeCalcul stub = (FormeCalcul) UnicastRemoteObject.exportObject(fc, 0);

		//Registry registry = LocateRegistry.getRegistry();
		Registry registry = LocateRegistry.createRegistry(1099);
		registry.rebind("calcul", stub);
		System.out.println("Serveur démarré");
	}

}
