package atelier.rmi.server;

import java.rmi.RemoteException;

import atelier.rmi.Cercle;
import atelier.rmi.Rectangle;

public class FormeCalculImpl implements FormeCalcul {

	@Override
	public double surface(Cercle cercle) throws RemoteException {
		return cercle.getRayon()*cercle.getRayon()*Math.PI;
	}

	@Override
	public double surface(Rectangle rectangle) throws RemoteException {
		return rectangle.getLargeur()*rectangle.getLongueur();
	}

}
