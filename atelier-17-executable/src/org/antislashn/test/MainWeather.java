package org.antislashn.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.antislashn.meteo.Weather;

public class MainWeather {

	public static void main(String[] args) {
		try {
			Map<String, Double> params = parseParams(args);
			if(params.size()!=2) {
				showInfos();
				return;
			}
			Weather weather = Weather.getInstance();
			String response = weather.weatherRequest(params.get("lat"),params.get("lng"));
			System.out.println(response);
		} catch(NumberFormatException e){
			showInfos();
		}
	}

	private static void showInfos() {
		System.out.println("Entrez les coordonnées sous la forme lat=xxx et lng=xxx\n exemple : lat=51.47846 lng=0.0");		
	}
	
	private static Map<String,Double> parseParams(String[] args){
		return Arrays.stream(args)
					.filter(s->s.startsWith("lat=")||s.startsWith("lng="))
					.map(s->s.split("="))
					.collect(Collectors.toMap(s->s[0],s->Double.parseDouble(s[1])));
	}

}
