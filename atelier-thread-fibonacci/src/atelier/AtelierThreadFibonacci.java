package atelier;

import java.math.BigInteger;

public class AtelierThreadFibonacci {

	public static void main(String[] args) throws InterruptedException {
		int nb = 10;
		FibonacciRunnable fr = new FibonacciRunnable(nb);
		Thread th = new Thread(fr);
		th.start();
		
		th.join();
		BigInteger resultat = fr.getResultat();
		
		System.out.printf("Fibonacci pour %d = %d\n",nb,resultat);

	}


}
