package atelier;

import java.math.BigInteger;

public class FibonacciRunnable implements Runnable {
	private BigInteger nb;
	private BigInteger suite;
	
	

	public FibonacciRunnable(int nb) {
		this.nb = BigInteger.valueOf(nb);
	}

	@Override
	public void run() {
		suite = recursif(nb);
		
	}
	
	public BigInteger getResultat() {
		return suite;
	}
	
	
	// ATTENTION les méthodes/fonctions récursives sont dangeureuses
	//				il peut y avoir un dépassement de pile potentiel si la récursivité est trop longue
	private BigInteger recursif(BigInteger nb) {
//		if(nb == 0 || nb == 1)
//			return nb;
//		return recursif(nb-1) + recursif(nb-2);
		
		if(nb.equals(BigInteger.ZERO) || nb.equals(BigInteger.ONE))
			return nb;
		return recursif(nb.subtract(BigInteger.ONE)).add(recursif(nb.subtract(BigInteger.TWO)));
	}
		
}
