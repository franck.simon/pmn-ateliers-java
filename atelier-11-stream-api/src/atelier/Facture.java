package atelier;

import java.util.ArrayList;
import java.util.List;

public class Facture {
	private String numero;
	private List<LigneFacture> lignesFacture = new ArrayList<>();

	public Facture() {}
	
	public Facture(String numero) {
		this.numero = numero;
	}
	
	public void add(LigneFacture ligneFacture) {
		lignesFacture.add(ligneFacture);
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public List<LigneFacture> getLignesFacture() {
		return lignesFacture;
	}

	public void setLignesFacture(List<LigneFacture> lignesFacture) {
		this.lignesFacture = lignesFacture;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("Facture : "+numero+"\n");
		lignesFacture.forEach(a->sb.append('\t').append(a).append('\n'));				
		return sb.toString();
	}

	
	
}
