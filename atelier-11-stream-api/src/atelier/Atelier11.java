package atelier;

import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Atelier11 {
	public static void main(String[] args) {
		List<Facture> factures = FactureDAO.getFactures();
		// factures.forEach(System.out::println);
		
		
		// récupération des lignes de factures car utilisé plusieurs fois
		List<LigneFacture> lignes = factures.stream()
										.map(f->f.getLignesFacture())
										.flatMap(List::stream)
										.collect(Collectors.toList());
		// Calculer
		//	montant total facturé
		//	le nombre total de produits vendus
		//	le min et max des facture
		//	facture moyenne
		
		// montant total facturé
		double total = lignes.stream().mapToDouble(l->l.getNombre()*l.getPrix()).sum();
		System.out.println("CA total : "+total);
		
		// nombre total de produits vendus
		double nbProduits = lignes.stream().mapToDouble(LigneFacture::getNombre).sum();
		System.out.println("Quantité vendue : "+nbProduits);
		
		
		
		// montant de chaque facture
//		Map<String, Double> facs = factures.stream()
//									.collect(Collectors.groupingBy(Facture::getNumero,
//											 Collectors.summingDouble(f->f.getLignesFacture().stream().collect(
//																Collectors.summingDouble(l->l.getPrix()*l.getNombre()))))); 
		Map<String, Double> facs = factures.parallelStream()
				.collect(Collectors.groupingBy(Facture::getNumero,
						 						Collectors.summingDouble(f->f.getLignesFacture().stream()
								 										 						.mapToDouble(l->l.getPrix()*l.getNombre())
								 										 						.sum()))); 
		
		facs.forEach((k,v)->System.out.println(k+" : "+v));
		
		// statistiques sur les factures
		DoubleSummaryStatistics stats = facs.values().stream().mapToDouble(Double::valueOf).summaryStatistics();
		System.out.println(stats);
	}

}
