package atelier;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;


public class FolderScanner extends RecursiveTask<Long> {

	private Path path = null;
	private String extension;
	private long result = 0;

	public FolderScanner() {
	}

	public FolderScanner(Path p, String extension) {
		path = p;
		this.extension = extension;
	}
	
	private long scan() throws ScanException{
		long nb = 0;
		// Si le chemin n'est pas valide
		if (path == null || path.equals(""))
			throw new ScanException("Chemin à scanner non valide (vide ou null) !");
		// Nous listons le contenu du répertoire
		try (DirectoryStream<Path> listing = Files.newDirectoryStream(path)) {
			// On parcourt le contenu
			for (Path path : listing) {
				// S'il s'agit d'un dossier, on crée une sous-tâche
				if (Files.isDirectory(path.toAbsolutePath())) {
					// Nous créons donc un nouvel objet FolderScanner
					// Qui se chargera de scanner ce dossier
					FolderScanner fScan = new FolderScanner(path.toAbsolutePath(), this.extension);
					fScan.fork(); 			// C'est cette instruction qui lance l'action en tâche de fond
					nb += fScan.join();		// on attend le résultat, puis ajoute au résultat
				}else {
					// s'il s'agit d'un fichier on regarde son extension
					if(path.toString().endsWith(extension)) {
						nb++;
					}
				}
			}
		} catch (IOException e) {
			throw new ScanException(e);
		}
		return nb;
	}

	@Override
	protected Long compute() {
		long resultat = 0;
		try {
			resultat = this.scan();
		} catch (ScanException e) {
			e.printStackTrace();
		}
		return resultat;
	}


	public long getResultat() {
		return this.result;
	}

}
