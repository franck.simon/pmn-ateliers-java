package atelier;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ForkJoinPool;

public class Atelier07 {
	public static void main(String[] args) throws ScanException {

		Path chemin = Paths.get("/home/franck/Formations");
	    String filtre = "odp";
	     
	    //Création de notre tâche principale qui se charge de découper son travail en sous-tâches
	    FolderScanner fs = new FolderScanner(chemin, filtre);
	     
	    //Nous récupérons le nombre de processeurs disponibles
	    int processeurs = Runtime.getRuntime().availableProcessors();
	    System.out.println("Nb de processeurs : "+processeurs);
	    //Nous créons notre pool de thread pour nos tâches de fond
	    ForkJoinPool pool = new ForkJoinPool(processeurs);
	    Long start = System.currentTimeMillis();
	     
	    //Nous lançons le traitement de notre tâche principale via le pool
	    Long nbFiles = pool.invoke(fs);

	    Long end = System.currentTimeMillis();
	    System.out.printf("\nIl y a %d fichier(s) portant l'extension %s\n",nbFiles,filtre);
	    System.out.printf("Temps de traitement : %d ms\n",(end - start));    
	}

}
